// Controllers dependencies
var users = require('./controllers/users');
var tokens = require('./controllers/tokens');
var articles = require('./controllers/articles');

// Authentication
function isAuthenticated(req, res, next) {
  tokens.authenticate(req, res, function(user) {
    req.user = user;
    return next();
  });
}

// Is authorized
function canEditArticle(req, res, next) {
  if (req.user._id !== req.article.author_id)
    return res.send(401, 'User is not authorized');

  return next();
}

// Routes
module.exports = function(router) {

  router.use(function(req, res, next) {
    // Middleware here
    next(); // make sure we go to the next routes and don't stop here
  });

  router.get('/', function(req, res) {
    res.json({ message: 'Hooray! Welcome to our Node.js API!' });
  });

  // Routes ending in /users
  router.route('/users')
    .post(users.create)
    .get(isAuthenticated, users.index);

  // Routes ending in /login
  router.route('/users/login')
    .post(users.login);

  // Routes ending in /logout
  router.route('/users/logout')
    .post(isAuthenticated, users.logout);

  // Routes ending in :user_id
  router.route('/users/:user_id')
    .get(isAuthenticated, users.get)
    .put(isAuthenticated, users.update)
    .delete(isAuthenticated, users.delete);

  // Routes ending in :user_id
  router.route('/users/:user_id/articles')
    .get(isAuthenticated, articles.getUsersArticles);

  // Routes ending in /articles
  router.route('/articles')
    .post(isAuthenticated, articles.create)
    .get(articles.index);

  // router.route('/articles')
  //   .delete(isAuthenticated, articles.deleteAll);

  // Routes ending in :article_id
  router.route('/articles/:article_id')
    .get(isAuthenticated, articles.get)
    .put(isAuthenticated, canEditArticle, articles.update)
    .delete(isAuthenticated, canEditArticle, articles.delete);

  router.param('article_id', articles.load);

};
