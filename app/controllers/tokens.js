// Model dependencies
var Token = require('../models/token');

exports.authenticate = function(req, res, cb) {

  if (!req.headers['x-auth-token']) {
    res.send(401);
    return;
  }

  Token.authenticate(req.headers['x-auth-token'], function(err, token) {
    if (err) {
      res.send(err);
      return;
    }

    if (!token) {
      res.send(401);
      return;
    }

    cb(token.user);
  });

};
