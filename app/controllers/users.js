// Utility dependencies
var passwordHash = require('password-hash');

// Model dependencies
var User = require('../models/user');
var Token = require('../models/token');

exports.create = function(req, res) {

  var user = new User();
  user.username = req.body.username;
  user.password = passwordHash.generate(req.body.password);

  user.save(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot save the user'
      });
    }

    res.json(user);
  });

};

exports.login = function(req, res) {

  User.findOne({
    username: req.body.username
  }, function(err, user) {
    if (err)
      return res.json(500, {
        error: 'Cannot login'
      });

    if (!user || !passwordHash.verify(req.body.password, user.password))
      res.send({
        type: 'error',
        message: 'Invalid username or password!'
      });

    // Create auth token, save it and return it if successfuly saved
    var token_hash = passwordHash.generate(req.body.username);

    var token = new Token();
    token.token = token_hash;
    token.user = user._id;

    // save the token and check for errors
    token.save(function(err) {
      if (err)
        res.send(err);

      res.json(token.token);
    });

  });

};

exports.index = function(req, res) {

  User.find(function(err, users) {
    if (err)
      res.send(err);

    res.json(users);
  });

};

exports.delete = function(req, res) {

  User.remove({
    _id: req.params.user_id
  }, function(err, user) {
    if (err)
      res.send(err);

    res.json({
      type: 'success',
      message: 'Successfully deleted'
    });
  });

};

exports.deleteTokens = function(user_id) {

  Token.remove({
    user: user_id
  }, function(err, tokens) {
    if (err)
      return false;

    return true;
  });

};

exports.update = function(req, res) {

  User.findById(req.params.user_id, function(err, user) {

    if (err)
      res.send(err);

    user.username = req.body.username;  // update the users info

    // save the user
    user.save(function(err) {
      if (err)
        res.send(err);

      res.json({
        type: 'success',
        message: 'User updated!'
      });
    });

  });

};

exports.get = function(req, res) {

  User.findById(req.params.user_id, function(err, user) {
    if (err)
      res.send(err);

    if (!user)
      res.json({
        type: 'error',
        message: 'Error loading user data'
      });

    res.json(user);
  });

};

exports.logout = function(req, res) {

  token_hash = req.headers['x-auth-token'];

  Token.remove({
    token: token_hash,
    user: req.user
  }, function(err, tokens) {
    if (err)
      res.send(err);

    res.json({
      type: 'success',
      message: 'Successfully logged out!'
    });
  });

};
