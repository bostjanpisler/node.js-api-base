// Utility dependencies
// ..

// Model dependencies
var Article = require('../models/article');
var _ = require('lodash');

// Initial load of article
exports.load = function(req, res, next, id) {
  Article.load(id, function(err, article) {
    if (err)
      return next(err);

    if (!article)
      return next(new Error('Failed to load article ' + id));

    req.article = article;
    next();
  });
};

// Create new article
exports.create = function(req, res) {

  var article = new Article();
  article.title = req.body.title;
  article.content = req.body.content;
  article.author_id = req.user._id;

  article.save(function(err) {
    if (err)
      return res.json(500, {error: 'Cannot save the article'});

    res.json(article);
  });
};

// Show all articles
exports.index = function(req, res) {

  Article.find(function(err, articles) {
    if (err)
      res.send(err);

    res.json(articles);
  });
};

// Delete article
exports.delete = function(req, res) {

  Article.remove({
    _id: req.params.article_id
  }, function(err, article) {
    if (err)
      res.send(err);

    res.json({
      type: 'success',
      message: 'Successfully deleted'
    });
  });
};

// Update article
exports.update = function(req, res) {

  var article = req.article;

  article = _.extend(article, req.body);

  article.save(function(err) {
    if (err)
      res.send(err);

    res.json({
      type: 'success',
      message: 'Article updated!'
    });
  });
};

// Get one article
exports.get = function(req, res) {

  Article.findById(req.params.article_id, function(err, article) {
    if (err)
      res.send(err);

    if (!article)
      res.json({
        type: 'error',
        message: 'Error loading article'
      });

    res.json(article);
  });
};

// Initial load of article
exports.getUsersArticles = function(req, res) {

  Article.loadUsersArticles(req.params.user_id, function(err, articles) {
    if (err)
      return next(err);

    if (!articles)
      return next(new Error('Failed to load articles'));

    res.json(articles);
  });
};

// Delete all articles
// exports.deleteAll = function(req, res) {

//   Article.deleteAll(function(err, result) {
//     if (err)
//       res.send(err);

//     if (!result)
//       res.send(200);

//     res.json(result);
//   });
// }
