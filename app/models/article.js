var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ArticleSchema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  content: {
    type: String,
    required: true,
    trim: true
  },
  author: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  }
});

/**
 * Validations
 */
ArticleSchema.path('title').validate(function(title) {
    return !!title;
}, 'Title cannot be blank');

ArticleSchema.path('content').validate(function(content) {
    return !!content;
}, 'Content cannot be blank');

/**
 * Statics
 */
// loadUsersArticles
ArticleSchema.statics = {

  load: function(id, cb) {
    this.find({ _id: id })
      .populate('author', 'id')
      .exec(cb);
  },

  loadUsersArticles: function(author_id, cb) {
    this.find({ author_id: author_id })
      .sort({'created_at': -1}) // sort by date
      .populate('author', 'id')
      .exec(cb);
  },

  deleteAll: function(cb) {
    this.find()
      .exec();

    this.remove()
      .exec(cb);
  }
}

module.exports = mongoose.model('Article', ArticleSchema);
