var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TokenSchema = new Schema({
  token: {
    type: String,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  created_at: {
    type: Date,
    default: Date.now
  }
});

/**
 * Statics
 */
TokenSchema.statics.authenticate = function(token, cb) {
  this.findOne({
    token: token
  })
    .populate('user', '_id username')
    .exec(cb);
};

module.exports = mongoose.model('Token', TokenSchema);
