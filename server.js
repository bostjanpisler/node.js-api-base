// BASE SERVER SETUP

// Call the packages we need
var express = require('express'); // Call express framework
var app = express(); // Define app using express
var port = process.env.PORT || 8080; // Set port
var router = express.Router(); // Routing
var mongoose = require('mongoose'); // Mongodb ORM
var config = require('./config/env/' + (process.env.NODE_ENV || 'development')); // Load environment based config
var bodyParser = require('body-parser'); // Request body content parser
var passwordHash = require('password-hash'); // Password hashing mechanism
var morgan = require('morgan'); // Logging

if (process.env.NODE_ENV === 'development')
  app.use(morgan('dev')); // Log every request to the console

app.use(bodyParser()); // Get data from post

// Set DB connection address
mongoose.connect(config.db_url);

// Load routes
require('./app/routes.js')(router);
app.use('/api', router); // All of the routes will be prefixed with /api

//  Middleware to use for all requests
// ...

// START THE SERVER
app.listen(port);
console.log('Magic happens on port ' + port);
